# ManicFootball
This is a public repository for my network programming coursework.

# Tools
-----------------------
* **Box2D  Physics Library**, available [here](https://github.com/erincatto/Box2D/releases/tag/v2.3.1).
* **SFML Visual C++ 12 (2013) - 64-bit**, available [here](http://www.sfml-dev.org/download/sfml/2.3.2/), download the version stated to make it compatible with the application.
